# Omnibus CNG Bridge

This repository is proof-of-concept for building CNG images from Omnibus definitions.

## Notes

1. Only Gitaly build is included.
1. A subset of Omnibus repository with some adjustments is used.
1. For ease of use the forked Omnibus is not used. Instead we use Omnibus 8.2 Gem in this PoC.

## How to use

### Step 1: Builder images

Build the builder images in `builder/` directory. Currently UBI and Debian variants are included.

### Step 2: Build Omnibus project

Run `docker run -v ${PWD}/omnibus:/build <BUILDER-IMAGE> bash`. This is to immitate a CI job.

Run the following commands:

```shell
bundle install
omnibus install gitaly
```

Grab the self-contained package script (`.sh`) from `omnibus/pkg` and copy it to `docker/gitaly/installer`.

### Step 3: Build Docker image

Build the Docker image in `docker/gitaly`. Currently UBI and Debian variants are included.

```shell
docker build -f Dockerfile.ubi -t gitaly-bridge:ubi .
```
