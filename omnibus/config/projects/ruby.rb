#
# Copyright 2022 YOUR NAME
#
# All Rights Reserved.
#

require "#{Omnibus::Config.project_root}/lib/gitlab/build_iteration"
require "#{Omnibus::Config.project_root}/lib/gitlab/build/info"
require "#{Omnibus::Config.project_root}/lib/gitlab/version"
require "#{Omnibus::Config.project_root}/lib/gitlab/util"
require "#{Omnibus::Config.project_root}/lib/gitlab/ohai_helper"
require "#{Omnibus::Config.project_root}/lib/gitlab/openssl_helper"

name 'ruby'
description 'GitLab Cloud Native Images'
maintainer 'GitLab, Inc. <support@gitlab.com>'
homepage 'https://about.gitlab.com/'

install_dir "#{default_root}/#{name}"

build_version Build::Info.semver_version
build_iteration Gitlab::BuildIteration.new.build_iteration

dependency 'ruby'

exclude '**/.git*'
exclude '**/bundler/git'
