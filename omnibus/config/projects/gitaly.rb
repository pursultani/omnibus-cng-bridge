#
# Copyright 2022 YOUR NAME
#
# All Rights Reserved.
#

require "#{Omnibus::Config.project_root}/lib/gitlab/build_iteration"
require "#{Omnibus::Config.project_root}/lib/gitlab/build/info"
require "#{Omnibus::Config.project_root}/lib/gitlab/version"
require "#{Omnibus::Config.project_root}/lib/gitlab/util"
require "#{Omnibus::Config.project_root}/lib/gitlab/ohai_helper"
require "#{Omnibus::Config.project_root}/lib/gitlab/openssl_helper"

name 'gitaly'
description 'GitLab Cloud Native Images'
maintainer 'GitLab, Inc. <support@gitlab.com>'
homepage 'https://about.gitlab.com/'

install_dir "#{default_root}/#{name}"
package_scripts_path "#{Omnibus::Config.project_root}/package-scripts/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

dependency 'ruby'
dependency 'gitaly'
dependency 'git'
dependency 'cng'

exclude '**/.git*'
exclude '**/bundler/git'

exclude 'embedded/lib/**/*.a'
exclude 'embedded/lib/**/*.la'
exclude 'embedded/include'
exclude 'embedded/man'
exclude 'embedded/share/doc'
exclude 'embedded/share/gtk-doc'
exclude 'embedded/share/info'
exclude 'embedded/share/man'
exclude 'embedded/lib/ruby/gems/*/cache'
exclude 'embedded/lib/ruby/gems/*/gems/*/spec'
exclude 'embedded/lib/ruby/gems/*/gems/*/test'
exclude 'embedded/lib/ruby/gems/*/gems/*/tests'
exclude 'embedded/lib/ruby/gems/*/extensions/*/*/*/mkmf.log'
exclude 'embedded/lib/ruby/gems/*/extensions/*/*/*/gem_make.out'
exclude 'embedded/lib/ruby/gems/*/gems/*/ext/*.c'
exclude 'embedded/lib/ruby/gems/*/gems/*/ext/*/*.c'
exclude 'embedded/lib/ruby/gems/*/gems/*/ext/*.o'
exclude 'embedded/lib/ruby/gems/*/gems/*/ext/*/*.o'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.gemspec'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.md'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.rdoc'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.sh'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.txt'
exclude 'embedded/lib/ruby/gems/*/gems/*/*.ruby'
exclude 'embedded/lib/ruby/gems/*/gems/*/*LICENSE*'
exclude 'embedded/lib/ruby/gems/*/gems/*/CHANGES*'
exclude 'embedded/lib/ruby/gems/*/gems/*/Gemfile'
exclude 'embedded/lib/ruby/gems/*/gems/*/Guardfile'
exclude 'embedded/lib/ruby/gems/*/gems/*/README*'
exclude 'embedded/lib/ruby/gems/*/gems/*/Rakefile'
exclude 'embedded/lib/ruby/gems/*/gems/*/run_tests.rb'

exclude 'embedded/lib/ruby/gems/*/gems/*/Documentation'
exclude 'embedded/lib/ruby/gems/*/gems/*/bench'
exclude 'embedded/lib/ruby/gems/*/gems/*/contrib'
exclude 'embedded/lib/ruby/gems/*/gems/*/doc'
exclude 'embedded/lib/ruby/gems/*/gems/*/doc-api'
exclude 'embedded/lib/ruby/gems/*/gems/*/examples'
exclude 'embedded/lib/ruby/gems/*/gems/*/fixtures'
exclude 'embedded/lib/ruby/gems/*/gems/*/gemfiles'
exclude 'embedded/lib/ruby/gems/*/gems/*/libtest'
exclude 'embedded/lib/ruby/gems/*/gems/*/man'
exclude 'embedded/lib/ruby/gems/*/gems/*/sample_documents'
exclude 'embedded/lib/ruby/gems/*/gems/*/samples'
exclude 'embedded/lib/ruby/gems/*/gems/*/sample'
exclude 'embedded/lib/ruby/gems/*/gems/*/script'
exclude 'embedded/lib/ruby/gems/*/gems/*/t'

exclude 'embedded/lib/ruby/gems/*/gems/grpc-*/include'
exclude 'embedded/lib/ruby/gems/*/gems/grpc-*/src/core'
exclude 'embedded/lib/ruby/gems/*/gems/grpc-*/src/ruby/ext'
exclude 'embedded/lib/ruby/gems/*/gems/grpc-*/src/ruby/spec'
exclude 'embedded/lib/ruby/gems/*/gems/grpc-*/third_party'
exclude 'embedded/lib/ruby/gems/*/gems/nokogumbo-*/ext'
exclude 'embedded/lib/ruby/gems/*/gems/rbtrace-*/ext/src'
exclude 'embedded/lib/ruby/gems/*/gems/rbtrace-*/ext/dst'
exclude 'embedded/lib/ruby/gems/*/gems/*pg_query-*/ext'
